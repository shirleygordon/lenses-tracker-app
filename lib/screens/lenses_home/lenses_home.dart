import 'package:flutter/material.dart';
import 'package:lenses_app/components/default_app_bar.dart';
import '../../components/default_app_bar.dart';

class LensesHome extends StatefulWidget {
  final String _name;

  LensesHome(this._name);

  @override
  State<StatefulWidget> createState() => _LensesHomeState();
}

class _LensesHomeState extends State<LensesHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(),
    );
  }
}
