import 'package:flutter/material.dart';
import 'package:lenses_app/screens/lenses_home/lenses_home.dart';
import 'screens/lenses_home/lenses_home.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LensesHome('Shirley'),
    );
  }
}
